//
//  ContentView.swift
//  DownloadingFromURL
//
//  Created by Leadconsultant on 11/13/19.
//  Copyright © 2019 Leadconsultant. All rights reserved.
//

import SwiftUI

struct Result:Codable {
    var trackId : Int
    var trackName : String
    var collectionName : String
}

struct Response:Codable {
    var results : [Result]
}

struct ContentView: View {
    @State var results = [Result]()
    var body: some View {
        List( results, id: \.trackId){item in
            VStack(alignment: .leading ){
                Text(item.trackName).font(.headline)
                Text(item.collectionName)
            }
        }.onAppear(perform: loadData)
    }
    
    func loadData(){
        guard let url = URL(string: "https://itunes.apple.com/search?term=taylor+swift&entity=song") else {
            print("No URL")
            return
        }
        
        let request = URLRequest(url: url)
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            //use Data
            if let data = data {
                if let decoredResponse = try? JSONDecoder().decode(Response.self, from:data){
                    DispatchQueue.main.async {
                        self.results = decoredResponse.results
                    }
                    return
                }
            }
            print("Fetch Failed")
        }.resume()
    }
}



struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
